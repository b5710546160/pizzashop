package pizzashop;
import java.util.*;
import java.util.function.Consumer;

import pizzashop.food.*;

/** A customer order containing food items.
 *  The iterator provides an iterator over the FoodItems in the order.
 *  The Observable notifies observers whenever the order changes.
 *  The FoodOrder itself is attached as an Observer of FoodItems so
 *  it knows when a FoodItem changes.   
 *   @author Kitipoom Kongpetch
 */
public class FoodOrder implements Iterable {
	/** list of items in the order. */
	private List<OrderItem> items;
	
	/** create a new, empty food order. */
	public FoodOrder() {
		items = new ArrayList();
	}
	/** add an item to the order.
	 *  @param order is the fooditem to add to order	
	 *  @return true if the item is successfully added
	 */
	public boolean add(OrderItem order) {
		items.add( order );
		return true;
	}
	/** remove an item from the order .
	 * @param item want to remove
	 * @return boolean*/
	public boolean remove(OrderItem item) {
		// return result of ArrayList.remove()
		boolean ok = items.remove( item );
		return ok;
	}
	/**
	 * clear order.
	 */
	public void clear() {
		items.clear();
	}
	
	/** get the total price of the order.
	 *  @return total price
	 */
	public double getPrice() {
		double price = 0;
		for( OrderItem obj : items ) {
			price +=obj.getPrice();
		}
		return price;
	}
	
	/** return an iterator for the items in the order.
	 * This is easy since ArrayList is already Iterable.
	 * @return Item
	 */
	public Iterator<? extends Object> iterator() {
		return items.iterator();
	}
	/**
	 * @return String
	 */
	public String toString() {
		StringBuffer s = new StringBuffer();
		for( OrderItem item : items ) s.append(item.toString()+"\n");
		return s.toString();
	}
	@Override
	public void forEach(Consumer action) {
		
	}
	@Override
	public Spliterator spliterator() {
		
		return null;
	}

}