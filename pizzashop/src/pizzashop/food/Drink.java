package pizzashop.food;

/** 
 * A drink has a size and flavor.
 * @author Kitipoom Kongpetch
 */
public class Drink extends AbstractItem implements OrderItem{
	// constants related to drink size
	public static final double [] prices = { 0.0, 20.0, 30.0, 40.0 };
	//public static final Size[] sizes = Size.values();
	private String flavor;
	private int size;
	/**
	 * create a new drink.
	 * @param size is the size. 1=small, 2=medium, 3=large
	 */
	public Drink( int size ) {
		this.size = size;
	}
	
	/**
	 * @return String
	 */
	public String toString() {
		return sizes[size] + " " + (flavor==null? "": flavor.toString()) + "drink"; 
	}

	/** return the price of a drink.
	 * @see pizzashop.FoodItem#getPrice()
	 * @return double
	 */
	public double getPrice() {
		if (size >= 0 && size < prices.length) return prices[size];
		return 0.0;
	}
	@Override
	public OrderItem clone() {
		Drink clone;
		
		try {
			clone = (Drink) super.clone();
		} catch (CloneNotSupportedException e) {
			System.err.println("Drink.clone: " + e);
			clone = new Drink(0);
		}
		//superclass already cloned the size and flavor
		return clone;
	}
}
