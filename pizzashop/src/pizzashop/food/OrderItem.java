package pizzashop.food;
/**
 * 
 * @author Kitipoom Kongpetch
 *
 */
public interface OrderItem {
	/**
	 * 
	 * @return String
	 */
	public String toString() ;
	/**
	 * 
	 * @return price
	 */
	public double getPrice() ;
	/**
	 * 
	 * @return OrderItem
	 */
	public OrderItem clone() ;
}
