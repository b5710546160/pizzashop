package pizzashop.food;
/**
 * 
 * @author Kitipoom Kongpetch
 *
 */
public enum Size {
	None("sone"), Small("small"), Medium("Medium"), Large("large");
	private String name;
	private Size(String a){
		this.name=a;
	}
	public String toString() { return name; }
}
