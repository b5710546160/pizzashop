package pizzashop.food;
/** Collection of the pizza toppings.
 * @author Kitipoom Kongpetch
 */
public enum Topping {
	VEGETABLE("vegetable", 15),
	MUSHROOM("mushroom", 15),
	SAUSAGE("sausage", 20),
	PINEAPPLE("pineapple", 10),
	TOFU("tofu", 15),
	NONE("none", 0);
	
	private Topping(String name, double price) {
		this.name = name;
		this.price = price;
	}
	
	private Topping(String name) {
		this.name = name;
	}
	
	private String name; // name of the topping
	private double price; // price of the topping
	
	/** @return description of the topping */
	public String toString() { return name; }
	/** @return the topping price */
	public double getPrice() { return price; }
}
